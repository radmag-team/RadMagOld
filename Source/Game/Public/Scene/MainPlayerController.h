// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECS/Components/Components.h"
#include "EnttIntegration/EnttWorld.h"
#include "GameFramework/PlayerController.h"
#include "InputCoreTypes.h"
#include "Interfaces/ListenerOfNextTurn.h"

#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class GAME_API AMainPlayerController : public APlayerController, public IListenerOfNextTurn
{
	GENERATED_BODY()

private:
	TQueue<RadMag::CMouseEvent> EventQueue;

protected:
	UFUNCTION(BlueprintCallable)
	void AddMousePressedEvent(FKey Key, FVector Location);

public:
	
	TOptional<RadMag::CMouseEvent> GetMouseEvent();
};
