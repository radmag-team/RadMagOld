// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInstance.h"
#include "Engine/StaticMesh.h"

#include "UnitController.generated.h"

UCLASS()
class GAME_API AUnitController : public AActor
{
	GENERATED_BODY()

protected:

	bool bInit;
	
	UPROPERTY()
	UStaticMeshComponent* MeshComponent;

public:
	AUnitController();

	void Init(UStaticMesh* SetMesh, UMaterialInstance* SetMaterial, const FTransform& Transform);
	void Move(FVector NewLocation);
};
