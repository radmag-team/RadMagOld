﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InputCoreTypes.h"
#include "Containers/StaticArray.h"
#include "entt/entt.hpp"

//You must add void GetTextInfo(FTextBuilder&) method for all components
namespace RadMag
{
	template <typename T>
	struct CActorPtr
	{
		T* Value;


		CActorPtr() = default;

		explicit CActorPtr(T* Value)
			: Value(Value)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("ActorPtr")));
			TextBuilder.AppendLine(FString(TEXT(" ")));
		}
	};

	struct CEntityName
	{
		FName Value;

		CEntityName()
		{
			Value = FName(TEXT("None"));
		}

		explicit CEntityName(const FName& Value)
			: Value(Value)
		{
		}

		friend bool operator==(const CEntityName& Lhs, const CEntityName& Rhs)
		{
			return Lhs.Value == Rhs.Value;
		}

		friend bool operator!=(const CEntityName& Lhs, const CEntityName& Rhs)
		{
			return !(Lhs == Rhs);
		}

		friend FORCEINLINE uint32 GetTypeHash(const CEntityName& EntityName)
		{
			return GetTypeHash(EntityName.Value);
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("Name: ")) + Value.ToString());
		}
	};

	struct CCubeCoordinate
	{
		FIntVector Value;

		CCubeCoordinate() = default;

		explicit CCubeCoordinate(const FIntVector& SetValue)
			: Value(SetValue)
		{
		}

		friend bool operator==(const CCubeCoordinate& Lhs, const CCubeCoordinate& Rhs)
		{
			return Lhs.Value == Rhs.Value;
		}

		friend bool operator!=(const CCubeCoordinate& Lhs, const CCubeCoordinate& Rhs)
		{
			return !(Lhs == Rhs);
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("Coordinate: ")) + Value.ToString());
		}

		static auto Distance(const CCubeCoordinate& Lhs, const CCubeCoordinate& Rhs) -> float
		{
			const auto Res = (Lhs.Value.X - Rhs.Value.X) * (Lhs.Value.X - Rhs.Value.X) +
				(Lhs.Value.Y - Rhs.Value.Y) * (Lhs.Value.Y - Rhs.Value.Y) +
				(Lhs.Value.Z - Rhs.Value.Z) * (Lhs.Value.Z - Rhs.Value.Z);
			return FMath::Sqrt(Res);
		}
	};

	struct CResource
	{
		CEntityName Name;
		int32 Count;

		CResource() = default;

		CResource(const CEntityName& Name, int32 Count)
			: Name(Name),
			  Count(Count)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(Name.Value.ToString() + FString(TEXT(": ")) + FString::FromInt(Count));
		}

		friend bool operator==(const CResource& Lhs, const CResource& RHS)
		{
			return Lhs.Name == RHS.Name;
		}

		friend bool operator!=(const CResource& Lhs, const CResource& RHS)
		{
			return !(Lhs == RHS);
		}
	};

	struct CFactory
	{
		CEntityName Name;
		TArray<CResource> BuildingInput;
		TArray<CResource> BuildingOutput;

		CFactory() = default;

		CFactory(const CEntityName& Name, const TArray<CResource>& BuildingInput,
		         const TArray<CResource>& BuildingOutput)
			: Name(Name),
			  BuildingInput(BuildingInput),
			  BuildingOutput(BuildingOutput)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(Name.Value.ToString());
			TextBuilder.AppendLine(FString(TEXT("Input:")));
			for (auto& Element : BuildingInput)
				Element.GetTextInfo(TextBuilder);
			TextBuilder.AppendLine(FString(TEXT("Output:")));
			for (auto& Element : BuildingOutput)
				Element.GetTextInfo(TextBuilder);
		}
	};

	struct CMine
	{
		CEntityName Name;
		TArray<CResource> BuildingOutput;

		CMine() = default;

		CMine(const CEntityName& Name, const TArray<CResource>& BuildingOutput)
			: Name(Name),
			  BuildingOutput(BuildingOutput)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(Name.Value.ToString());
			TextBuilder.AppendLine(FString(TEXT("Output:")));
			for (auto& Element : BuildingOutput)
				Element.GetTextInfo(TextBuilder);
		}
	};

	struct CWorldInfo
	{
		uint32 CurrentTurn;

		CWorldInfo() = default;

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("CurrentTurn: ")) + FString::FromInt(CurrentTurn));
		}
	};

	struct CHexData
	{
		CEntityName BiomeName;
		TArray<CResource> Resources;

		CHexData() = default;

		auto AddResources(CResource Res, int32 Count) -> void
		{
			Res.Count = Count;
			Resources.Add(Res);
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("Biome: ")) + BiomeName.Value.ToString());
			for (auto& Element : Resources)
				Element.GetTextInfo(TextBuilder);
		}
	};

	struct CCityData
	{
		TArray<CFactory> Factories;
		TArray<CMine> Mines;
		TArray<CResource> Resources;

		CCityData() = default;

		CCityData(const TArray<CFactory>& Factories, const TArray<CMine>& Mines)
			: Factories(Factories),
			  Mines(Mines)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			for (auto& Element : Factories)
				Element.GetTextInfo(TextBuilder);
			TextBuilder.AppendLine(FString(TEXT(" ")));
			for (auto& Element : Mines)
				Element.GetTextInfo(TextBuilder);
			TextBuilder.AppendLine(FString(TEXT(" ")));
			for (auto& Element : Resources)
				Element.GetTextInfo(TextBuilder);
		}

		auto ResourceContains(const TArray<CResource>& InputResources) const -> bool
		{
			for (auto& Element : InputResources)
			{
				const auto Id = Resources.Find(Element);
				if (Id == INDEX_NONE)
					return false;
				if (Resources[Id].Count < Element.Count)
					return false;
			}

			return true;
		}

		auto DeleteResources(const TArray<CResource>& InputResources) -> void
		{
			for (auto& Element : InputResources)
			{
				const auto Id = Resources.Find(Element);
				check(Id != INDEX_NONE);
				Resources[Id].Count -= Element.Count;
			}
		}

		auto AddResources(const TArray<CResource>& InputResources) -> void
		{
			for (auto& Element : InputResources)
			{
				const auto Id = Resources.Find(Element);
				if (Id == INDEX_NONE)
				{
					Resources.Add(Element);
					continue;
				}
				Resources[Id].Count += Element.Count;
			}
		}
	};

	struct CUnitData
	{
		int32 MaxAP;
		int32 AP;

		CUnitData() = default;

		CUnitData(int32 MaxAP)
			: MaxAP(MaxAP),
			  AP(MaxAP)
		{
		}
	};

	struct CMoveOrder
	{
		CCubeCoordinate Value;

		CMoveOrder() = default;

		explicit CMoveOrder(const CCubeCoordinate& Value)
			: Value(Value)
		{
		}
	};

	struct CMouseEvent
	{
		FKey Key;
		FVector Location;

		CMouseEvent() = default;

		CMouseEvent(const FKey& Key, const FVector& Location)
			: Key(Key),
			  Location(Location)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("MouseEvent")));
			TextBuilder.AppendLine(FString(TEXT("Key: ")) + Key.ToString());
			TextBuilder.AppendLine(FString(TEXT("Location: ")) + Location.ToString());
			TextBuilder.AppendLine(FString(TEXT(" ")));
		}
	};

	struct CDedicatedUnit
	{
		entt::entity Value;

		CDedicatedUnit() = default;

		explicit CDedicatedUnit(entt::entity Value)
			: Value(Value)
		{
		}
	};
}
