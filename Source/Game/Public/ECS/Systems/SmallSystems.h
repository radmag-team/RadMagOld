﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ECS/Components/Components.h"
#include "EnttIntegration/EnttWorld.h"
#include "ECS/Others/BasicTemplates.h"
#include "ECS/Resources/Resources.h"

namespace RadMag
{
	template<typename Tag>
	FORCEINLINE auto NameContains(CEntityName Name, const FEnttData& EnttData) -> bool
	{
		auto Vi = EnttData.GetView(entt::type_list<Tag, CEntityName>{});
		auto It = std::find_if(Vi.begin(), Vi.end(), TEquals(Vi, Name));
		return It != Vi.end();
	}

	FORCEINLINE auto IsCorrectResourceMap(TMap<FName, int32>& Map, const FEnttData& EnttData) -> bool
	{
		const auto& CheckList = EnttData.GetResource<FResourceList>();
		//Check exist resource in input/output map
		for (const auto Element : Map)
		{
			if (!CheckList.Contains(CEntityName(Element.Key))
				|| Element.Value < 0)
				return false;
		}
		return true;
	}
}
