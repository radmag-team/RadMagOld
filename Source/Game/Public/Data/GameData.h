// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InitContext.h"
#include "Engine/DataAsset.h"
#include "Engine/StaticMesh.h"
#include "GameData.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UGameData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditInstanceOnly)
	FCreateGameRulesInitContext GameRules;
	
	UPROPERTY(EditInstanceOnly)
	TArray<FCreateResourceInitContext> Resources;

	UPROPERTY(EditInstanceOnly)
	TArray<FCreateFactoryInitContext> Factories;

	UPROPERTY(EditInstanceOnly)
	TArray<FCreateMineInitContext> Mines;

	UPROPERTY(EditInstanceOnly)
	FCreateUnitInitContext Unit;
};
