// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/TextViewerWidget.h"
#include "Blueprint/UserWidget.h"
#include "EnttIntegration/EnttWorld.h"
#include "WidgetWithEnttPtr.generated.h"

class UTextViewerWidget;
/**
 * 
 */
UCLASS(Abstract)
class GAME_API UWidgetWithEnttPtr : public UUserWidget
{
	GENERATED_BODY()

private:
	RadMag::FEnttWorld* EnttWorld;

public:

	UFUNCTION(BlueprintCallable)
    void Init();

	UFUNCTION(BlueprintCallable)
	void GetWorldInfo(TScriptInterface<ITextViewerWidget> Widget);

	UFUNCTION(BlueprintCallable)
    void GetMapInfo(TScriptInterface<ITextViewerWidget> Widget);

	UFUNCTION(BlueprintCallable)
    void GetHexInfo(TScriptInterface<ITextViewerWidget> Widget, const FVector& Location);

	UFUNCTION(BlueprintCallable)
    void GetAllResourceInfo(TScriptInterface<ITextViewerWidget> Widget);

	UFUNCTION(BlueprintCallable)
    void GetAllBuildingInfo(TScriptInterface<ITextViewerWidget> Widget);
	
	UFUNCTION(BlueprintCallable)
	void NextTurn();
};
