// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/MainPlayerController.h"
#include "Engine/Engine.h"

void AMainPlayerController::AddMousePressedEvent(FKey Key, FVector Location)
{
	EventQueue.Enqueue(RadMag::CMouseEvent(Key, Location));
}

TOptional<RadMag::CMouseEvent> AMainPlayerController::GetMouseEvent()
{
	if(EventQueue.IsEmpty())
		return TOptional<RadMag::CMouseEvent>();

	RadMag::CMouseEvent Event;
	EventQueue.Dequeue(Event);
	return TOptional(Event);
}
