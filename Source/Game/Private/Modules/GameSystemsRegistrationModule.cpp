// Fill out your copyright notice in the Description page of Project Settings.

#include "Modules/GameSystemsRegistrationModule.h"
#include "ECS/Systems/Systems.h"

void UGameSystemsRegistrationModule::SetECSWorld(RadMag::FEnttWorld* SetWorld)
{
	EnttWorldPtr = SetWorld;
}

void UGameSystemsRegistrationModule::Initialize()
{
	check(EnttWorldPtr);
	EnttWorldPtr->AddSystem<&RadMag::NextTurn>();
	EnttWorldPtr->AddSystem<&RadMag::ParseInput>();
	EnttWorldPtr->AddSystem<&RadMag::MoveUnit>();
}
