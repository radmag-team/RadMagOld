﻿
#include "EnttLibrary/Public/EnttLibraryModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FEnttLibraryModule, EnttLibrary);

DEFINE_LOG_CATEGORY(EnttLibrary);
 
#define LOCTEXT_NAMESPACE "EnttLibrary"
 
void FEnttLibraryModule::StartupModule()
{
	UE_LOG(EnttLibrary, Warning, TEXT("EnttLibrary module has started!"));
}
 
void FEnttLibraryModule::ShutdownModule()
{
	UE_LOG(EnttLibrary, Warning, TEXT("EnttLibrary module has shut down"));
}
 
#undef LOCTEXT_NAMESPACE